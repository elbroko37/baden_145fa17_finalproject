import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class EndGame2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class EndGame2 extends Buttons
{
    /**
     * This is the constructor for the EndGame2 class
     */
    public EndGame2()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() - 300, image.getHeight() - 300 ); 
        setImage(image); 
    } // end constructor EndGame2
    /**
     * Act - do whatever the EndGame2 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    } // end method act   
} // end class EndGame2
