import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The user controls a character battling depression.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class User extends Actor
{
    /* instance variables*/

    /**
     * This is the constructor for the class User.
     */
    public User()
    {
        // the image was to small. this code makes it larger
        // I got part of the following code from "https://www.greenfoot.org/topics/1763"
        // this code is in the other classes in order to resize them.
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 100, image.getHeight() + 100 ); // increases image
        setImage(image);                                                // by 100 pixles
    } // end constructor User

    /**
     * Act - do whatever the User wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move();
        rotate();
        kill();
    } // end method act  

    /**
     * Check to see if a key is down. This will allow you to 
     * make your character move how you want it to. 
     */
    public void move()
    {
        if ( Greenfoot.isKeyDown("left") )
        {
            setLocation(getX()-6, getY());
        }
        else if ( Greenfoot.isKeyDown("right") )
        {
            setLocation(getX()+6, getY());
        }
        else if ( Greenfoot.isKeyDown("down") )
        {
            setLocation(getX(), getY()+6);
        }
        else if ( Greenfoot.isKeyDown("up") )
        {
            setLocation(getX(), getY()-6);
        } // end multi-way if/else
    } // end method checkKeypress

    /**
     * Rotate - rotate the character to the direction that it moves.
     */
    public void rotate()
    {
        if ( Greenfoot.isKeyDown("left") )
        {
            setRotation(90);
        }
        else if ( Greenfoot.isKeyDown("right") )
        {
            setRotation(-90);
        } 
        else if ( Greenfoot.isKeyDown("down") )
        {
            setRotation(0);
        }
        else if ( Greenfoot.isKeyDown("up") )
        {
            setRotation(180);
        } // end mutiway if/else
    } // end method rotate
    
    /**
     * Kill - this method will allow the User to "eat" incoming enemies.
     */
    private void kill()
    {
       if ( isTouching(Positive.class) )
        {
            removeTouching(Positive.class);
            GameWorld world = (GameWorld) getWorld();
            world.totalScore(-10);
        }
        else if ( isTouching(Emptiness.class) )
        {
            removeTouching(Emptiness.class);
            GameWorld world = (GameWorld) getWorld();
            world.totalScore(5);
        }
        else if ( isTouching(Grief.class) )
        {
            removeTouching(Grief.class);
            GameWorld world = (GameWorld) getWorld();
            world.totalScore(5);
        }
        else if ( isTouching(Thought.class) )
        {
            removeTouching(Thought.class);
            GameWorld world = (GameWorld) getWorld();
            world.totalScore(20); 
        } // end multi-way if/else 
    } // end method kill
} // end class User
