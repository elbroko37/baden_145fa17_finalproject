import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The snakes in this gamerepresent depression. The user must defeat them.
 * 
 * @author Baden Bennett 
 * @version 1
 */
public class Emptiness extends Actor
{
    
    /**
     * This is the Emptiness constructor.
     */
    public Emptiness()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 80, image.getHeight() + 80 );
        setImage(image); 
    } // end constructor Emptiness
    
    /**
     * Act - do whatever the Emptiness wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move(1);
        turnTowards(325, 325);
        getImage().rotate(-2);
    } // end method act 
} // end class Snake
