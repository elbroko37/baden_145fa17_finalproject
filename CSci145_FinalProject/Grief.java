import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * One cause of suicide through depression is grief.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Grief extends Actor
{
    /**
     * This is the constructor for thGrief class
     */
    public Grief()
    {
        // I got part of the following code from "https://www.greenfoot.org/topics/1763"
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 60, image.getHeight() + 60 );
        setImage(image); 
    } // end constructor Grief
    
    /**
     * Act - do whatever the Grief wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move(2);
        turnTowards(325, 325);
        getImage().rotate(5);
    } // end method act   
} // end class Grief
