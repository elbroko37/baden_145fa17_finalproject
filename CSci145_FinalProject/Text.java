import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Text here.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class Text extends Actor
{
    /**
     * This is the constructor for the Text class.
     */
    public Text()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 330, image.getHeight() + 330 ); 
        setImage(image); 
    } // end constructor Text
} // end class Text
