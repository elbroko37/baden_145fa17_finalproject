import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the game Menu, you can choose difficulties here and view the controls.
 * 
 * @author Baden Benett 
 * @version 1
 */
public class Menu extends World
{
    private GreenfootSound bgMusic;
    /**
     * Constructor for objects of class Menu.
     * 
     */
    public Menu()
    {    
        // Create a new world with 650x650 cells with a cell size of 1x1 pixels.
        super(650, 650, 1);

        GreenfootImage background = new GreenfootImage("MenuTitle.png");
        background.scale( getWidth(), getHeight() );
        setBackground(background);
        
        // background music
        bgMusic = new GreenfootSound("loop_main.wav");
        bgMusic.setVolume(100);
        
        
        prepare();
        started();
        stopped();

        setPaintOrder(Buttons.class);

        dispOption();
    } // end constructor Menu

    /**
     * Place the objects into the world.
     */
    public void prepare()
    {
        Buttons button = new Buttons();
        addObject(button, 180, 350);
        Buttons button2 = new Buttons();
        addObject(button2, 180, 460);
        Buttons button3 = new Buttons();
        addObject(button3, 180, 570);
        Buttons button4 = new Buttons();
        addObject(button4, 500, 350);
        
        Text text = new Text();
        addObject(text, 220, 265);
        
        Soul soul = new Soul();
        addObject(soul, 520, 500);
    } // end prepare Menu

    /**
     * When the game is started do the following.
     */
    public void started()
    {
        bgMusic.playLoop(); // playmusic in an infinite loop until stopped
    } // end method started
    
    /**
     * When the game is stopped, do the following.
     */
    public void stopped()
    {
        bgMusic.stop(); // stop the music when the game is stopped. 
                        // if the game is started back, play the
                        // loop from the begining.
    } // end method stopped
    
    /**
     * Display the difficulty options.
     */
    private void dispOption()
    {
        //showText("", 180, 230);
        showText("Rich Old Lady " + "(Q)", 180, 330); 
        showText("Female " + "(A)", 145, 440);
        showText("Male " + "(Z)", 135, 550);
        showText("Controls" + "(C)" , 465, 330);
    } // end method dispOption

    /**
     * This is the act method for class Menu.
     */
    public void act()
    {
        checkSetting();
    } // end method act

    /**
     * This method will instantiate the world that we choose from the menu.
     */
    public void checkSetting()
    {
        if ( Greenfoot.isKeyDown( "z" ) )
        {
            Greenfoot.playSound("click_sound.wav");
            Greenfoot.setWorld( new GameWorld() );
        } 
        else if ( Greenfoot.isKeyDown( "a" ) )
        {
            Greenfoot.playSound("click_sound.wav");
            Greenfoot.setWorld( new GameWorld2() );
        }
        else if ( Greenfoot.isKeyDown( "q" ) )
        {
            Greenfoot.playSound("click_sound.wav");
            Greenfoot.setWorld( new GameWorld3() );
        }
        else if ( Greenfoot.isKeyDown( "c" ) )
        {
            Greenfoot.playSound("click_sound.wav");
            Greenfoot.setWorld( new Controls() );
        }// end multi-way if/else
    } // end method checkSetting
} // end class Menu
