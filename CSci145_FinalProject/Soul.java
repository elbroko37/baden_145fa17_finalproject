import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the soul of the character. It must be protected.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class Soul extends Actor
{
    /**
     * This is the constructor for the Soul class.
     */
    public Soul()
    {
        // the image was to small. this code makes it larger
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 180, image.getHeight() + 180 ); // increases image
                                                                        // by 180 pixles
        setImage( image );
    } // end constructor Soul
    
    /**
     * Act - do whatever the Soul wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
      turn(1);
    } // end method act 
} // end class Soul
