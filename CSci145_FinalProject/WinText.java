import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WinText here.
 * 
 * @author Baden Bennett 
 * @version 1
 */
public class WinText extends Text
{
    /**
     * This is the constructor for the WinText class.
     */
    public WinText()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 250, image.getHeight() + 250 ); 
        setImage(image); 
    } // end constructor WinText   
} // end class WinText
