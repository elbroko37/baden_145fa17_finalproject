import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This screen is displayed when the player loses the game.
 * 
 * @author Baden Bennett 
 * @version 1
 */
public class GameOver extends World
{
    /* instance variables */
    private GreenfootSound bgMusic;
    
    /**
     * Constructor for objects of class GameOver.
     * 
     */
    public GameOver()
    {    
        // Create a new world with 650x650 cells with a cell size of 1x1 pixels.
        super(650, 650, 1); 
        // size background
        GreenfootImage background = new GreenfootImage("gameOver.png");
        background.scale( getWidth(), getHeight() );
        setBackground(background);
        
        // background music
        bgMusic = new GreenfootSound("dark_ambience.wav");
        bgMusic.setVolume(100);
        bgMusic.play();
        
        prepare();
    } // end constructor GameOver
    
    /**
     * This method will prepare the world.
     */
    public void prepare()
    {
        EndGame endgame = new EndGame();
        addObject(endgame, 500, 600);
    } // end method prepare
    
    /**
     * Act - this method will allow the world to "act"
     */
    public void act()
    {
        checkKeypress();
    } // end method act
    
    /**
     * CeckKeypress - this  method will see if a key is being pressed.
     */
    private void checkKeypress()
    {
        if ( Greenfoot.isKeyDown("enter") )
        {
            Greenfoot.setWorld( new Menu() );
            Greenfoot.playSound("click_sound.wav");
            bgMusic.stop();
        } // end if
    } // end method checkKeypress
} // end class GameOver
