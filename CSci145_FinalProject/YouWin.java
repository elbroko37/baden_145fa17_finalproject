import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This screen displays when the player wins the game
 * 
 * @author Baden Bennett
 * @version 1
 */
public class YouWin extends World
{

    /**
     * Constructor for objects of class YouWin.
     * 
     */
    public YouWin(int score)
    {    
        // Create a new world with 650x650 cells with a cell size of 1x1 pixels.
        super(650, 650, 1); 

        GreenfootImage background = new GreenfootImage("background_finalProject.png");
        background.scale( getWidth(), getHeight() );
        setBackground(background);

        prepare();
        
        setPaintOrder(EndGame2.class);
        dispScore(score);
    } // end constructor YouWin

    /**
     * This method will prepare the world with objects.
     */
    public void prepare()
    {
        EndGame2 endgame = new EndGame2();
        addObject(endgame, 500, 600);
        
        Soul soul = new Soul();
        addObject(soul, 325, 325);
        
        Edge edge = new Edge();
        addObject(edge, 325, 325);
        
        WinText text = new WinText();
        addObject(text, 320, 325);
    } // end method prepare
    
    /**
     * Display score.
     */
    public void dispScore(int score)
    {
        showText("Final Score: " + score , 325, 450 );
    } // end method dispScore
    
    /**
     * Act - this method will allow the world to "act"
     */
    public void act()
    {
        checkKeypress();
    } // end method act
    
    /**
     * CeckKeypress - this  method will see if a key is being pressed.
     */
    private void checkKeypress()
    {
        if ( Greenfoot.isKeyDown("enter") )
        {
            Greenfoot.setWorld( new Menu() );
            Greenfoot.playSound("click_sound.wav");
        } // end if
    } // end method checkKeypress
} // end class YouWin
