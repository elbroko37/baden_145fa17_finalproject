import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Edge here.
 * 
 * @author Baden Bennett 
 * @version 1
 */
public class Edge extends Actor
{
    /**
     * This is the constructor for the edge class
     */
    public Edge()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 700, image.getHeight() + 700 ); // increases image
        setImage(image);
    } // end constructor Edge
} // end class Edge
