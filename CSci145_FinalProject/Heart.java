import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Heart here.
 * 
 * @author Baden Bennett 
 * @version 1
 */
public class Heart extends Actor
{
    /**
     * This is the constructor for the Heart class.
     */
    public Heart()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 100, image.getHeight() + 100 ); // increases image
        setImage(image);
    } // end constructor Heart
} // end class heart
