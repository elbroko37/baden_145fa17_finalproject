import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Depression here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Thought extends Actor
{
    /**
     * This is the constructor for Thought.
     */
    public Thought()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 50, image.getHeight() + 50 );
        image.rotate( -90 );
        setImage(image); 
    } // end constructor suicidalThought
    
    /**
     * Act - do whatever the Thought wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
       move(3);
       turnTowards(325, 325);
    } // end method act   
} // end class suicidalThought
