import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class User2 here.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class User2 extends User
{
    /**
     * Act - do whatever the User2 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move();
        rotate(); 
        kill();
    } // end method act 

    /**
     * Kill - this methdo will allow User 2 to "kill" incoming enemies.
     */
    private void kill()
    {
        if ( isTouching(Positive.class) )
        {
            removeTouching(Positive.class);
            GameWorld2 world = (GameWorld2) getWorld();
            world.totalScore(-10);
        }
        else if ( isTouching(Emptiness.class) )
        {
            removeTouching(Emptiness.class);
            GameWorld2 world = (GameWorld2) getWorld();
            world.totalScore(5);
        }
        else if ( isTouching(Grief.class) )
        {
            removeTouching(Grief.class);
            GameWorld2 world = (GameWorld2) getWorld();
            world.totalScore(5);
        }
        else if ( isTouching(Thought.class) )
        {
            removeTouching(Thought.class);
            GameWorld2 world = (GameWorld2) getWorld();
            world.totalScore(15); 
        } // end multi-way if/else 
    } // end method kill
} // end class User2
