import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Positives will boost your score, just don't touch them.
 * 
 * @author Baden Bennett 
 * @version 1
 */
public class Positive extends Actor
{
    /**
     * This is the constructor for the Positive class
     */
    public Positive()
    {
       // the image was to small. this code makes it larger
       GreenfootImage image = getImage();
       image.scale( image.getWidth() + 50, image.getHeight() + 50 ); // increases image
                                                                     // by 50 pixles
       setImage( image ); 
    } // end constructor Positive
    
    /**
     * Act - do whatever the Positive wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move(1);
        turnTowards(320, 325);
        getImage().rotate(2);
    } // end method act  
} // end class Positive
