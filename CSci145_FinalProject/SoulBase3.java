import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This verson of the object will be instantiated into GamWorld3.
 * 
 * @author Baden Bennett 
 * @version 1
 */
public class SoulBase3 extends SoulBase
{
    /**
     * Act - do whatever the SoulBase3 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
       checkCollision();
    } // end method act
    
    /**
     * This method will check to see if the object 
     * is touching any other objects. 
     */
    private void checkCollision()
    {
        if ( isTouching(Positive.class) )
        {
            removeTouching(Positive.class);
            GameWorld3 world = (GameWorld3) getWorld();
            world.totalScore(371);
        }
        else if ( isTouching(Emptiness.class) )
        {
            removeTouching(Emptiness.class);
            GameWorld3 world = (GameWorld3) getWorld();
            world.totalScore(-5);
            world.soulLife(-1);
        }
        else if ( isTouching(Grief.class) )
        {
            removeTouching(Grief.class);
            GameWorld3 world = (GameWorld3) getWorld();
            world.totalScore(-5);
            world.soulLife(-1);
        }
        else if ( isTouching(Thought.class) )
        {
            removeTouching(Thought.class);
            GameWorld3 world = (GameWorld3) getWorld();
            world.soulLife(-1);
        } // end multi-way if/else
       } // end method checkCollision
} // end class SoulBase3
