import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class User3 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class User3 extends User
{
    /**
     * Act - do whatever the User3 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move();
        rotate();
        kill();
    } // end method act  

    /**
     * Kill - this method will allow the User to "eat" incoming enemies.
     */
    private void kill()
    {
        if ( isTouching(Positive.class) )
        {
            removeTouching(Positive.class);
            GameWorld3 world = (GameWorld3) getWorld();
            world.totalScore(-5);
        }
        else if ( isTouching(Emptiness.class) )
        {
            removeTouching(Emptiness.class);
            GameWorld3 world = (GameWorld3) getWorld();
            world.totalScore(5);
        }
        else if ( isTouching(Grief.class) )
        {
            removeTouching(Grief.class);
            GameWorld3 world = (GameWorld3) getWorld();
            world.totalScore(5);
        }
        else if ( isTouching(Thought.class) )
        {
            removeTouching(Thought.class);
            GameWorld3 world = (GameWorld3) getWorld();
            world.totalScore(30); 
        } // end multi-way if/else 
    }// end method kill 
} // end class User3
