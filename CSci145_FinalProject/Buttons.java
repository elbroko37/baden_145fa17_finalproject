import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Buttons here.
 * 
 * @author Baden Bennett 
 * @version 1
 */
public class Buttons extends Actor
{
    /**
     * This is the constructor for the button class.
     */
    public Buttons()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() + 500, image.getHeight() + 500 ); 
        setImage(image);  
    } // end constructor Buttons
} // end class Buttons
