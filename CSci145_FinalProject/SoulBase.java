import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class SoulBase here.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class SoulBase extends Actor
{
    /**
     * Act - do whatever the SoulBase wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkCollision();
    } // end method act

    /**
     * This method will check to see if the object 
     * is touching any other objects. 
     */
    private void checkCollision()
    {
        if ( isTouching(Positive.class) )
        {
            removeTouching(Positive.class);
            GameWorld world = (GameWorld) getWorld();
            world.totalScore(40);
        }
        else if ( isTouching(Emptiness.class) )
        {
            removeTouching(Emptiness.class);
            GameWorld world = (GameWorld) getWorld();
            world.totalScore(-5);
            world.soulLife(-1);
        }
        else if ( isTouching(Grief.class) )
        {
            removeTouching(Grief.class);
            GameWorld world = (GameWorld) getWorld();
            world.totalScore(-5);
            world.soulLife(-1);
        }
        else if ( isTouching(Thought.class) )
        {
            removeTouching(Thought.class);
            GameWorld world = (GameWorld) getWorld();
            world.soulLife(-1);
        } // end multi-way if/else
       } // end method checkCollision
    } // end class SoulBase
