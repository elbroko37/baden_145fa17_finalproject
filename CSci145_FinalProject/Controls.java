import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Controls here.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class Controls extends World
{

    /**
     * Constructor for objects of class Menu.
     * 
     */
    public Controls()
    {    
        // Create a new world with 650x650 cells with a cell size of 1x1 pixels.
        super(650, 650, 1);

        GreenfootImage background = new GreenfootImage("background_finalProject.png");
        background.scale( getWidth(), getHeight() );
        setBackground(background);

        prepare();
    } // end constructor Controls
    
    /**
     * 
     */
    public void prepare()
    {
        showText("Use the arrow keys to move and keep the enemies \n" + 
                 "from reaching the Soul in the middle of the world. \n" +
                 "If you survive the allotted time, You Win, \n" +
                 "but if too many enemies reach the center before time \n" +
                 "runs out, you lose." , 325, 200 );
        
        EndGame2 endgame = new EndGame2();
        addObject(endgame, 500, 600);
        
        Soul soul = new Soul();
        addObject(soul, 100, 550);
    } // end method prepare
    
    /**
     * Act - this method will allow the world to "act"
     */
    public void act()
    {
        checkKeypress();
    } // end method act
    
    /**
     * CeckKeypress - this  method will see if a key is being pressed.
     */
    private void checkKeypress()
    {
        if ( Greenfoot.isKeyDown("enter") )
        {
            Greenfoot.setWorld( new Menu() );
            Greenfoot.playSound("click_sound.wav");
        } // end if
    } // end method checkKeypress
} // end class  
