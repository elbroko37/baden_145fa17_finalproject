import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class EndGame here.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class EndGame extends Buttons
{
    /**
     * This is the constructor for the EndGame class.
     */
    public EndGame()
    {
        GreenfootImage image = getImage();
        image.scale( image.getWidth() - 300, image.getHeight() - 300 ); 
        setImage(image);  
    } // end constructor EndGame
    
    /**
     * Act - do whatever the EndGame wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // nothing right now
    } // end method act    
} // end class EndGame
