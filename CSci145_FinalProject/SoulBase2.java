import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This verson of the object will be instantiated into GamWorld2.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class SoulBase2 extends SoulBase
{
    /**
     * Act - do whatever the SoulBase2 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkCollision();
    } // end method act
    
    /**
     * This method will check to see if the object 
     * is touching any other objects. 
     */
    private void checkCollision()
    {
        if ( isTouching(Positive.class) )
        {
            removeTouching(Positive.class);
            GameWorld2 world = (GameWorld2) getWorld();
            world.totalScore(40);
        }
        else if ( isTouching(Emptiness.class) )
        {
            removeTouching(Emptiness.class);
            GameWorld2 world = (GameWorld2) getWorld();
            world.totalScore(-5);
            world.soulLife(-1);
        }
        else if ( isTouching(Grief.class) )
        {
            removeTouching(Grief.class);
            GameWorld2 world = (GameWorld2) getWorld();
            world.totalScore(-5);
            world.soulLife(-1);
        }
        else if ( isTouching(Thought.class) )
        {
            removeTouching(Thought.class);
            GameWorld2 world = (GameWorld2) getWorld();
            world.soulLife(-1);
        } // end multi-way if/else
       } // end method checkCollision
} // end class SoulBase2
