import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the game world in which the the difficulty "Female" is played.
 * 
 * @author Baden Bennett
 * @version 1
 */
public class GameWorld2 extends World
{
    /* instance variables */
    private int score;
    private int soulHealth;
    private int time = 1500;
    /**
     * Constructor for objects of class GameWorld2.
     * 
     */
    public GameWorld2()
    {    
        // Create a new world with 650x650 cells with a cell size of 1x1 pixels.
        super(650, 650, 1);

        // Make the background fit the game world dimensions.
        GreenfootImage background = new GreenfootImage("background_finalProject.png");
        background.scale( getWidth(), getHeight() );
        setBackground(background);

        prepare();
        setPaintOrder( Soul.class ); 

        score = 0;
        soulHealth = 5;

        dispScore();
        dispSoulHealth();
        dispTime();
    } // end constructor GameWorld2
    
    /**
     * This is the act method for class GameWorld.
     */
    public void act()
    {
        spawnEmptiness();
        spawnGrief();
        spawnThought();
        spawnPositive();
        timer();
    } // end method act

    /**
     * Prepare the world at the start of the game.
     */
    private void prepare()
    {
        User2 user = new User2();
        addObject(user, 325, 460);

        Soul soul = new Soul();
        addObject(soul, 325, 325);

        SoulBase2 base = new SoulBase2();
        addObject(base, 325, 325);

        Edge edge = new Edge();
        addObject(edge, 325, 325);
        
        Heart heart = new Heart();
        addObject(heart, 590, 30);
    } // end method prepare

    /**
     * SpawnEmptiness - This method will spawn Emptiness souls into the world.
     */
    private void spawnEmptiness()
    {

        if ( Greenfoot.getRandomNumber(300) < 1 )
        {
            addObject( new Emptiness(), 650, Greenfoot.getRandomNumber(650) );
            addObject( new Emptiness(), 0, Greenfoot.getRandomNumber(650) );
            addObject( new Emptiness(), Greenfoot.getRandomNumber(650), 0 );
            addObject( new Emptiness(), Greenfoot.getRandomNumber(650), 650 );
        } // end if
    } // end method spawnEmptiness

    /**
     * spawnGrief - This method will spawn Grief souls into the world.
     */
    private void spawnGrief()
    {

        if ( Greenfoot.getRandomNumber(300) < 1 )
        {
            addObject( new Grief(), 650, Greenfoot.getRandomNumber(650) );
            addObject( new Grief(), 0, Greenfoot.getRandomNumber(650) );
            addObject( new Grief(), Greenfoot.getRandomNumber(650), 0 );
            addObject( new Grief(), Greenfoot.getRandomNumber(650), 650 );
        } // end if
    } // end method spawnGrief

    /**
     * Thos method will spawn "suicidal" Thoughts into the world.
     */
    private void spawnThought()
    {
        if ( Greenfoot.getRandomNumber(500) < 1 )
        {
            addObject( new Thought(), 650, Greenfoot.getRandomNumber(650) );
            addObject( new Thought(), 0, Greenfoot.getRandomNumber(650) );
            addObject( new Thought(), Greenfoot.getRandomNumber(650), 0 );
            addObject( new Thought(), Greenfoot.getRandomNumber(650), 650 );
        } // end if
    } // end method spawnThought

    /**
     * SpawnPositive - This method will spawn positives into the world. 
     */
    private void spawnPositive()
    {

        if ( Greenfoot.getRandomNumber(300) < 1 )
        {
            addObject( new Positive(), 650, Greenfoot.getRandomNumber(650) );
            addObject( new Positive(), 0, Greenfoot.getRandomNumber(650) );
            addObject( new Positive(), Greenfoot.getRandomNumber(650), 0 );
            addObject( new Positive(), Greenfoot.getRandomNumber(650), 650 );
        } // end if
    } // end method spawnPositive

    /**
     * This method will calculate the user's score.
     */
    public void totalScore(int points)
    {
        score = score + points;
        dispScore();
    } // end method totalScore

    /**
     * This method will display the score onto the world. 
     */
    private void dispScore()
    {
        showText("Morale: " + score, 65, 25);
    } // end method dispScore

    /**
     * This method will calculate how much health the Soul has left. 
     */
    public void soulLife(int hits)
    {
        soulHealth = soulHealth + hits;
        dispSoulHealth();
        if ( soulHealth == 0 )
        {
            Greenfoot.setWorld( new GameOver() );
            Greenfoot.playSound("gunshot.wav");
        } // end if
    } // end method soulLife

    /**
     * This method will display the amount of health the soul has left.
     */
    private void dispSoulHealth()
    {
        showText(" " + soulHealth, 570, 25);
    } // end method dispSoulHealth
    
    /**
     * Thsis method will countdown the time the the user has to stay alive for.
     */
    private void timer()
    {
        time = time - 1;
        dispTime();
        if ( time < 1 )
        {
            Greenfoot.setWorld( new YouWin(score) );
        } // end if
    } // end method timer
    
    /**
     * This method will display the time left in the game.
     */
    private void dispTime()
    {
        showText("Time: "+ time, 325, 25);
    } // end method dispTime
} // end class GameWorld2
