# Baden_145fa17_FinalProject

Author: Baden Bennett
Date: 12/14/2017
Version: 1
User Instructions: -Use the arrow keys to move your mouse character.	
		   -Keep enemies away from the Soul in the center of the world by running     into them 
                    and "killing" them.
		   -If too many enemies reach the soul, you lose.
                   -Survive the allotted time to win the game and collect your points.
		   -Have Fun!
